package com.company.AC_Actividades.Actividad15;

import java.util.Scanner;

public class NIF {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Ingrese su nif.");
        String nif = scanner.next();

        NIF checkNif = new NIF(nif);

        if (checkNif.isValid()) {
            System.out.println("\"Bienvenido\"");
        }else {
            System.out.println("No es valido.");
        }
    }

    private String nif;

    public NIF(String nif) {
        this.nif = nif;
    }

    public  boolean isValid() {
        return nif.matches("[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKE]");
    }
}
