package com.company.AC_Actividades.Actividad19;

import com.company.AC_Actividades.Actividad18.Validator;

import java.util.Scanner;

public class ControllerManager {

    Scanner in = new Scanner(System.in);
    Validator validator = new Validator();

    private Person person;


    public Person newPerson() {

        String name;
        String lastname;
        String dni;
        String zipCod;
        String email;

        do {
            System.out.print("Ingrese su nombre:  ");
            name = in.nextLine();
            if (!validator.isValidName(name)) {
                System.out.println("Esto esta incorrecto ");
            }
        }
        while (!validator.isValidName(name));

        do {
            System.out.print("Ingrese su apellido:  ");
            lastname = in.nextLine();
            if (!validator.isValidLastName(lastname)) {
                System.out.println("Esto esta incorrecto ");
            }
        }
        while (!validator.isValidLastName(lastname));

        do {
            System.out.print("Ingrese su DNI: ");
            dni = in.nextLine();
            if (!validator.isValidNIF(dni)) {
                System.out.println("Estos esta incorreco");
            }
        }
        while (!validator.isValidNIF(dni));

        do {
            System.out.print("Ingrese su codigo postal: ");
            zipCod = in.nextLine();
            if (!validator.isValidZip(zipCod)) {
                System.out.println("Estos esta incorreco");
            }
        }
        while (!validator.isValidZip(zipCod));

        do {
            System.out.print("Ingrese correo electronico: ");
            email = in.nextLine();
            if (!validator.isValidMail(email)) {
                System.out.println("Estos esta incorreco");
            }
        }
        while (!validator.isValidMail(email));

        return new Person(name,lastname,dni,zipCod,email);
    }

    public Person modPerson(Person employee) {
        return this.person = newPerson();
    }
}