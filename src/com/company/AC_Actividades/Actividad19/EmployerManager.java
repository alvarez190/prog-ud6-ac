package com.company.AC_Actividades.Actividad19;

import java.util.Scanner;

public class EmployerManager {

    private final int OP_VIEW_DATA = 1;
    private final int OP_CHANGE_DATA = 2;
    private final int OP_GO_OUT = 3;

    private Scanner in = new Scanner(System.in);

    private Person person;
    private ControllerManager employerController = new ControllerManager();

    EmployerManager(Person person) {
        this.person = person;
        welcome();
        menu();
    }

    EmployerManager() {
        welcome();
        completeForm();
    }

    private void completeForm() {
        if (person == null) {
            System.out.println("Actualmente no hay datos, desea introducirlos(S/N).");

            char character = in.nextLine().charAt(0);
            character = Character.toLowerCase(character);

            if (character == 's') {
                addData();
            }
            else if (character == 'n'){
                menu();
            }
        }
    }
    private void addData() {
        this.person = employerController.newPerson();
        menu();
    }
    private void modData() {
        this.person = employerController.modPerson(person);
        menu();
    }

    private void welcome() {
        System.out.println("Bienvenido al programa \"Employer Manager\" v.1.0");
    }

    private void menu() {
        System.out.println("1) Visualizar datos." + "\n" +
                "2) Cambiar datos." + "\n" +
                "3) Salir.");

        int res = in.nextInt();

        if (res == OP_VIEW_DATA) {
            System.out.println(person.toString());
            menu();
        } else if (res == OP_CHANGE_DATA) {
            modData();

        } else if (res == OP_GO_OUT) {
            System.out.println("Hasta luego");
        }
    }

}
