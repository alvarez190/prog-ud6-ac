package com.company.AC_Actividades.Actividad19;

public class Person {

    private String nombre;
    private String apellido;
    private String dni;
    private String zipCod;
    private String email;

    public Person() {
    }

    public Person(String nombre, String apellido, String dni, String zipCod, String email) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.zipCod = zipCod;
        this.email = email;
    }

    public String toString(){
        return nombre + "\n" +
                apellido + "\n" +
                dni + "\n" +
                zipCod + "\n" +
                email+"\n";
    }
}
