package com.company.AC_Actividades.Actividad18;

public class Main {
    public static void main(String[] args) {
        Validator validator = new Validator();

        System.out.println( validator.isValidZip("46701") + "\n" +
        validator.isValidNIF("12345678Y")+ "\n" +
        validator.isValidMail("prueba_123@gmail.com") + "\n" +
        validator.isValidMac("40:8D:5C:27:D7:47") + "\n" +
        validator.isValidIp("192.168.1.110"));
    }
}
