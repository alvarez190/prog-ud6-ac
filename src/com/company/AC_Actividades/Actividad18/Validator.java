package com.company.AC_Actividades.Actividad18;

public class Validator {

    public Validator(){
    }

    public boolean isValidName(String name) {
        return name.matches("[a-zA-z]+");
    }

    public boolean isValidLastName(String lastName) {
        return lastName.matches("[a-zA-z]+");
    }

    public boolean isValidZip(String zip) {
        return zip.matches("^([0-4][0-9]|[5][0-2])[0-9]{3}");
    }

    public boolean isValidNIF(String nif) {
        return nif.matches("[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKE]");
    }

    public boolean isValidMail(String mail) {
        return mail.matches("^\\w{2,30}@[a-zA-z]+\\.(com|es)");
    }

    public boolean isValidMac(String mac) {
        return mac.matches("^([0-9]|[a-zA-F]){2}(:([0-9]|[a-zA-F]){2}){5}");
    }

    public boolean isValidIp(String ip) {
        return ip.matches("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$");
    }
}
