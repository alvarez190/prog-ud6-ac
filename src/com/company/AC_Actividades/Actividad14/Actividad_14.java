package com.company.AC_Actividades.Actividad14;

import java.util.Scanner;

public class Actividad_14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Ingresa tu cuenta para comprobarla.");
        String account = scanner.nextLine();

        AccountBank accountBank = new AccountBank(account);

        if (accountBank.isValid()) {
            System.out.println("\"Bienvenido.\"");
        }else {
            System.out.println("No es valido.");
        }
    }
}
