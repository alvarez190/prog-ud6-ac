package com.company.AC_Actividades.Actividad14;

public class AccountBank {

    private String account;

    public AccountBank(String account) {
        this.account = account;
    }

    public boolean isValid() {
        return account.matches("[0-9]{4}\\-[0-9]{4}\\-[0-9]{2}\\-[0-9]{10}");
    }
}
