package com.company.AC_Actividades.Actividad13;

import java.util.Scanner;

public class Actividad_13 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Introduce tu codigo postal: ");
        String codePostal = scanner.next();

        CheckCodePostal checkCodePostal = new CheckCodePostal(codePostal);

        if (checkCodePostal.isValid()) {
            System.out.println("\"Bienvenido\"");
        }else {
            System.out.println("El codigo no es valido");
        }
    }
}
