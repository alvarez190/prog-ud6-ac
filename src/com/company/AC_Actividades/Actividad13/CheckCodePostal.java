package com.company.AC_Actividades.Actividad13;

public class CheckCodePostal {

    private String code;

    public CheckCodePostal(String code) {
        this.code = code;
    }

    public boolean isValid() {
        return code.matches("^([0-4][0-9]|[5][0-2])[0-9]{3}");
    }
}
