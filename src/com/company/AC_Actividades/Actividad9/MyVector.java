package com.company.AC_Actividades.Actividad9;

public class MyVector {
    public static void main(String[] args) {

        int[] vectorV = new int[50];
        int[] vectorP = new int[20];

        int counter = 1;

        //Relleno los vectores
        for (int i = 0; i < vectorV.length; i++) {
            vectorV[i] = counter;
            counter++;
        }

        counter = 1;

        for (int i = 0; i < vectorP.length; i++) {
            vectorP[i] = counter;
            counter++;
        }

        showMatriz(createMatriz(vectorV,vectorP));
    }

    private static int[][] createMatriz (int[] v, int[] p) {

        int[][] matriz = new int[v.length][p.length];

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                matriz[i][j] = v[i] * p [j];
            }
        }
        return matriz;
    }

    private static void showMatriz (int[][] matriz) {
        for (int i = 0; i < matriz.length; i++) {
            System.out.println(" ");
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print(matriz[i][j] + " | ");
            }
        }
    }
}
