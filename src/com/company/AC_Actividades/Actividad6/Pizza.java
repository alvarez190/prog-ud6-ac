package com.company.AC_Actividades.Actividad6;

public class Pizza {

    private final int MAX = 5;
    private Mass mass;
    private Ingredient[] ingredients;
    private float prize;

    public Pizza(Mass mass, Ingredient ingredient) {
        this.ingredients = new Ingredient[MAX];
        this.mass = mass;
        this.ingredients[0] = ingredient;
    }


    public Pizza(Mass mass, Ingredient... ingredient) {
        this.ingredients = new Ingredient[ingredient.length];
        this.mass = mass;

        for (int i = 0; i < ingredients.length; i++) {
            this.ingredients[i] = ingredient[i];
        }
    }

    public float getPrize() {

        for (int i = 0; i < ingredients.length; i++) {
            prize += ingredients[i].getPrize();
        }
        return mass.getPrize() + prize;
    }
}
