package com.company.AC_Actividades.Actividad6;

public class Main {
    public static void main(String[] args) {

        Mass gruesa_relleno = new Mass("gruesa con relleno de queso");

        Ingredient tomate = new Ingredient("tomate");
        Ingredient mozzarela = new Ingredient("mozzarela");
        Ingredient jamon = new Ingredient("jamon york");

        Pizza pizza1 = new Pizza(gruesa_relleno,tomate,mozzarela,jamon);

        System.out.println("El total de la pizza es " + pizza1.getPrize() + "€" );
    }
}
