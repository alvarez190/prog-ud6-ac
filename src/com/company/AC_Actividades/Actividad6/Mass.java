package com.company.AC_Actividades.Actividad6;

public class Mass {
    private String mass;
    private float prize;

    public Mass(String mass) {
        this.mass = mass.toLowerCase();
        compareMass();
    }

    private void compareMass() {
        switch (mass){
            case "fina":
                prize = 2.5f;
                break;
            case "gruesa":
                prize = 3.5f;
            case "gruesa con relleno de queso":
                prize = 5f;
        }
    }

    public float getPrize() {
        return prize;
    }
}
