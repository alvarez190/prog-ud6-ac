package com.company.AC_Actividades.Actividad6;

public class Ingredient {

    private String ingredient;
    private float prize;

    Ingredient(String ingredient) {
        this.ingredient = ingredient.toLowerCase();
        compareIngredient();
    }

    private void compareIngredient() {
        switch (ingredient) {
            case "tomate":
                prize = 3.5f;
                break;
            case "mozzarela":
                prize = 4f;
                break;
            case "queso azul":
                prize = 4.5f;
                break;
            case "jamon york":
                prize = 2.5f;
                break;
            case "chedar":
                prize = 3f;
                break;
            case "queso base":
                prize = 1.5f;
                break;
            default:
                System.out.println("No tenemos ese ingrediente.");
        }
    }

    public float getPrize() {
        return prize;
    }
}
