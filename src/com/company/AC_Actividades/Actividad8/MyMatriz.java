package com.company.AC_Actividades.Actividad8;

public class MyMatriz {

    private int[][] matriz;

    public MyMatriz(int[][] matriz) {
        this.matriz = matriz;
    }

    public int[][] createNewMatriz() {
        int plusNumber;

        int[][] newMatriz = new int[7][7];

        for (int i = 0; i < matriz.length; i++) {
            plusNumber = 0;
            for (int j = 0; j < matriz[i].length; j++) {
                newMatriz[i][j] = matriz[i][j];
                plusNumber += matriz[i][j];
            }
            newMatriz[i][5] = plusNumber;
            newMatriz[i][6] = plusNumber/2;
        }
        return newMatriz;
    }

    public void showFirstMatriz () {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print(matriz[i][j] + " | ");
            }
            System.out.println(" ");
        }
    }

    public void showNewMatriz (int[][] newMatriz) {
        for (int i = 0; i < newMatriz.length; i++) {
            for (int j = 0; j < newMatriz[i].length; j++) {
                System.out.print(newMatriz[i][j] + " | ");
            }
            System.out.println(" ");
        }
    }
}
