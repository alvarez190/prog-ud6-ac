package com.company.AC_Actividades.Actividad8;

public class Main {
    public static void main(String[] args) {

        int counter = 1;

        int[][] matriz = new int[7][5];

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                matriz[i][j] = counter;
                counter++;
            }
        }

        MyMatriz myMatriz = new MyMatriz(matriz);

        System.out.println("Visualizacion de la primera matriz.");
        myMatriz.showFirstMatriz();

        System.out.println("\nVisualizacion de la nueva matriz");
        myMatriz.showNewMatriz(myMatriz.createNewMatriz());

    }
}
