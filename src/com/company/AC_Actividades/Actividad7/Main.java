package com.company.AC_Actividades.Actividad7;

public class Main {
    public static void main(String[]args) {

        final int line = 5;
        final int column = 7;

        int[][] matriz = new int[line][column];

        int counter = 1;

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                matriz[i][j] = counter;
                counter++;
            }
        }

        MyMatriz myMatriz = new MyMatriz(matriz);

        myMatriz.showMatrix();
        myMatriz.showLine5();
        myMatriz.showElement4_5x3AndSwap();
        myMatriz.showColumn1_4AndSwap();
    }
}
