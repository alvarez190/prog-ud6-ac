package com.company.AC_Actividades.Actividad7;

public class MyMatriz {
    private int[][] matriz;

    public MyMatriz(int[][] matriz) {
        this.matriz = matriz;
    }

    public void showMatrix() {
        System.out.println("Visualizacion de la matriz completa.");
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print(matriz[i][j] + " | ");
            }
            System.out.println(" ");
        }
    }

    public void showLine5() {
        System.out.println("\n\nVisualizacion de los elementos de la linea 5.\n");

        for (int j = 0; j < matriz.length; j++) {
            System.out.print(matriz[4][j] + " | ");
        }

    }

    public void showElement4_5x3AndSwap() {
        int aux;

        System.out.println("\n\nVisualizacion del elemento de 4-5 fila y columna 3\n" +
                "(4x3) "+ matriz[3][2] + " |\n"+
                "(5x3) " + matriz[4][2] + " |");

        System.out.println("Se intercambia los valores.\n" +
                "la posicion (4x3) por " + matriz[4][2] + "\n" +
                "y la posicion (5x3) por " + matriz[3][2] + "\n");

        //Intercambiamos los valores.
        aux = matriz[3][2];
        matriz[3][2] = matriz[4][2];
        matriz[4][2] = aux;

        showMatrix();
    }

    public void showColumn1_4AndSwap() {

        System.out.println("Visualizacion de los elementos de la columna 1 y 4");

        for (int i = 0; i < matriz.length; i++) {
            System.out.println(matriz[i][0] + " | " + matriz[i][3]);
        }

        int aux;

        System.out.println("Intercambiamos valores.");

        for (int i = 0; i < matriz.length; i++) {
            aux = matriz[i][0];
            matriz[i][0] = matriz[i][3];
            matriz[i][3] = aux;

            System.out.println(matriz[i][0] + " | " + matriz[i][3]);
        }

        showMatrix();
    }
}
