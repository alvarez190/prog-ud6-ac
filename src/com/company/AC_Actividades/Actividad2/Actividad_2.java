package com.company.AC_Actividades.Actividad2;

import java.util.Scanner;

public class Actividad_2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Introduce 10 elementos alfanumericos");

        String[] array = new String[10];

        for (int i = 0; i < array.length; i++) {
            System.out.print((i+1) + ". ");
            array[i] = scanner.next();
        }

        showPositionParAndOdd(array);

    }

    private static void showPositionParAndOdd(String[] array) {

        System.out.println("Aqui tienes los elementos que se ubican en posicion par.");
        for (int i = 0; i < array.length; i+=2) {
            System.out.print(i + "-> " + array[i] + " | ");
        }

        System.out.println("\nAqui tienes los elementos que se ubican en posicion impar.");
        for (int i = 1; i < array.length; i+=2) {
            System.out.print(i + "-> " + array[i] + " | ");
        }
    }
}
