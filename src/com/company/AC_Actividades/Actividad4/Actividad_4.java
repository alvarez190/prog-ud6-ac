package com.company.AC_Actividades.Actividad4;

import java.util.Scanner;

public class Actividad_4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] array = new int[50];

        plusNumberAndParNumber(completeArrayWithRandomNumber(array));
    }

    private static int[] completeArrayWithRandomNumber(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random()*(50-1+1)+1);
        }
        return array;
    }

    private static int[] completeWithKeyboard(int[] array, Scanner scanner) {
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        return array;
    }

    private static void plusNumberAndParNumber (int[] array) {
        int plusTotal = 0;
        int plusNumberPar = 0;

        for (int i = 0; i < array.length; i++) {
            plusTotal += array[i];

            if (array[i] % 2 == 0) {
                plusNumberPar += array[i];
            }
        }

        System.out.println("La suma total de los elementos del array es " + plusTotal + "\n" +
                "La suma de los elementos par es " + plusNumberPar);

    }
}
