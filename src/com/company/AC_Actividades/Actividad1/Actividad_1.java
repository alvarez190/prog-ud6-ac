package com.company.AC_Actividades.Actividad1;

import java.util.Scanner;

public class Actividad_1 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int[] array = new int[10];

        System.out.println("Inserta 10 numeros.");

        for (int i = 0; i < array.length; i++) {
            System.out.print((i+1)+ ". ");
            array[i] = scanner.nextInt();
        }

        showArray(array);
    }

    private static void showArray(int[] array) {

        System.out.println("Aqui tienes una visualizacion de tu array.");
        for (int i = 0; i < array.length; i++) {
            System.out.println((i+1) + ". " + array[i]);
        }
    }
}
