package com.company.AC_Actividades.Actividad17;

import java.util.Scanner;

public class PhoneNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Escribe un numero de telefono.");
        String phoneNumber = scanner.next();

        if (isValid(phoneNumber)) {
            System.out.println("\"Bienvenido\"");
        }else {
            System.out.println("No es valido.");
        }
    }

    private static boolean isValid (String phoneNumber) {
        return phoneNumber.matches("^(([3][4]|[0]{2}[3][4]|\\+[3][4])|([9][0-9]|[9][0-9]{2}))[0-9]{8}");
    }
}
