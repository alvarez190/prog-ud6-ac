package com.company.AC_Actividades.Actividad16;

import java.util.Scanner;

public class MobileNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Ingresa tu numero de telefono");
        String phoneNumber = scanner.nextLine();

        if (isValid(phoneNumber)) {
            System.out.println("\"Bienvenido\"");
        }else {
            System.out.println("No es valido");
        }
    }

    private static boolean isValid(String number) {
        return number.matches("^(([3][4]|[0]{2}[3][4]|\\+[3][4])|([6]|[7]))[0-9]{8}");
    }
}
