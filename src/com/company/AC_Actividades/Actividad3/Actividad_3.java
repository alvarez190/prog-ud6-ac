package com.company.AC_Actividades.Actividad3;

import java.util.Scanner;

public class Actividad_3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] array = new int[21];

        System.out.println("Introduce 20 numeros.");

        for (int i = 1; i < array.length; i++) {
            System.out.print(i + ". ");
            array[i] = scanner.nextInt();
        }

        //Visualizo los elementos del array en 4 en 4
        for (int i = 4; i < array.length; i+=4) {
            System.out.print(i +"-> "+ array[i] + " | ");
        }
    }
}