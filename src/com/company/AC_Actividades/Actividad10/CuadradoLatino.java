package com.company.AC_Actividades.Actividad10;

public class CuadradoLatino {

    private int factor;
    private int[][] myArray;

    CuadradoLatino(int factor) {

        this.factor = factor;

        this.myArray = new int[factor][factor];

        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[i].length; j++) {
                if (i == 0) {
                    myArray[i][j] = j + 1;
                } else if (j == 0) {
                    myArray[i][j] = myArray[i - 1][factor - 1];
                } else {
                    myArray[i][j] = myArray[i - 1][j - 1];
                }
            }
        }
    }

    public void showMatriz() {

        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[i].length; j++) {
                System.out.print(myArray[i][j]+" ");
            }
            System.out.println("\n");
        }
    }

}
