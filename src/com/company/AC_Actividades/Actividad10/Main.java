package com.company.AC_Actividades.Actividad10;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Dime de que tamaño quieres ver el cuadro latino.");
        int answer = scanner.nextInt();

        CuadradoLatino cuadradoLatino = new CuadradoLatino(answer);

        cuadradoLatino.showMatriz();

    }
}
