package com.company.AC_Actividades.Actividad12;

public class Regex {

    private String phrase;

    public Regex(String phrase) {
        this.phrase = phrase;
    }

    public void findLetterThatRepeats() {
        char aux = ' ';
        int counter1 = 0;
        int counter2;

        for (int i = 0; i < phrase.length(); i++) {
            counter2 = 0;
            for (int j = 1; j < phrase.length(); j++) {
                aux = phrase.charAt(i);

                if (aux == phrase.charAt(j)) {
                    counter2++;
                }
            }
            if (counter2 > counter1 ) {
                counter1 = counter2;
            }
        }

        String[] array = phrase.split(" ");

        int counter3 = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i].charAt(array[i].length()-1) == aux) {
                counter3++;
            }
        }

        System.out.println("El caracter \"" + aux +"\" a aparecido mas veces en un total de "+ counter1 + " y  hay " + counter3 + " palabras que terminan con \"" + aux + "\"");
    }
}
