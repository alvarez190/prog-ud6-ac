package com.company.AC_Actividades.Actividad12;

import java.util.Scanner;

public class Actividad_12 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Ingresa una frase.");
        String phrase = scanner.nextLine();

        Regex regex = new Regex(phrase);

        regex.findLetterThatRepeats();
    }
}
