package com.company.AC_Actividades.Actividad5;

public class MyArray {

    private int[] array;

    public MyArray(int[] array) {
        this.array = array;
    }

    public void showArray() {
        for (int i = 0; i < array.length; i++) {
            System.out.print(i+ "-> " + array[i] + " | ");
        }
    }

    public void showElementPar() {
        for (int i = 0; i < array.length; i += 2) {
            System.out.print(i+ "-> " + array[i] + " | ");
        }
    }

    public void showElementOdd() {
        for (int i = 1; i < array.length; i+= 2) {
            System.out.print(i+ "-> " + array[i] + " | ");
        }
    }
}
