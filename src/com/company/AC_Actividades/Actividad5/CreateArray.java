package com.company.AC_Actividades.Actividad5;

import java.util.Scanner;

public class CreateArray {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("\"Bienvenido\"\n" +
                "Crearemos un array.\n" +
                "Inserta el numero de elementos que deseas inserta: ");
        int numbers = scanner.nextInt();

        System.out.println("Bien ahora inserta "+ numbers + " numeros.");
        int[] array = new int[numbers];

        for (int i = 0; i < array.length; i++) {
            System.out.print((i+1) + ". ");
            array[i] = scanner.nextInt();
        }

        MyArray myArray = new MyArray(array);

        System.out.println("\nVisualizacion completa del array.");
        myArray.showArray();

        System.out.println("\nVisualizacion de elementos par y su posicion");
        myArray.showElementPar();

        System.out.println("\nVisualizacion de elementos impar con su posicion del array");
        myArray.showElementOdd();

    }
}
